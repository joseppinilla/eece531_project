'''
Windows System Variables
DFTB_PREFIX /home/jose/dftbplus/external/slakos/origin/mio-1-1/
DFTB_COMMAND wsl /home/jose/dftbplus/_install/bin/dftb+


'''
import os
import numpy as np
from math import sqrt
from copy import copy, deepcopy
# Atomic Simulation Environment (ASE)
from ase import Atoms
from ase.io import write, read
from ase.build import fcc100, diamond100, add_adsorbate, stack, root_surface, add_vacuum
from ase.geometry import get_layers
from ase.optimize import QuasiNewton
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.calculators.dftb import Dftb

a_0 = 5.2918e-11        # Bohr radius (m)
eV = 1.6021766208e-19   # ElectronVolt (J)

# Bond Lengths In Angstrom
H_H  = 3.5
Si_H_top = 1.48
Si_H_bot = sqrt(1.48**2-(1.92/2)**2)

def buildSystem():
    #Face Centered Cubic crystal system
    size = (6,6,10)
    z_vacuum = 1
    si100 = diamond100('Si',size, vacuum=z_vacuum)
    x_layers = get_layers(si100,(1,0,0))
    y_layers = get_layers(si100,(0,1,0))
    z_layers = get_layers(si100,(0,0,1))
    #Add vacuum
    y_vacuum = 1.0
    si100.center(y_vacuum, axis=1)

    #Add adsorbate on top of silicon slab
    x_offset = x_layers[1][1]
    for x in range(size[0]):
        for y in range(size[1]):
            add_adsorbate(si100, 'H', Si_H_top, position=(x_offset,y_vacuum), offset=(x,y))

    slab = si100.copy()

    z_bottom = - (z_layers[1][-1] - z_vacuum) - Si_H_bot
    #Add adsorbate at bottom of silicon slab
    for x in range(size[0]):
        for y in range(2*size[1]):
            H_pos = (2*x_layers[1][x], y_layers[1][y])
            add_adsorbate(slab, 'H', z_bottom, position=H_pos)

    #view(slab,viewer='vmd') # change to CPK representation
    write('crystal.xyz', slab)

    return slab

def geOpt_H(system):

    calc = Dftb(    label='SiH',
                    atoms=system,
                    run_manyDftb_steps=True,
                    #Hamiltonian_SCC ='YES',
                    Driver_='ConjugateGradient',
                    #Driver_MaxForceComponent='0.02',
                    Driver_MaxSteps=50,
                    #Driver_MovedAtoms='H',
                    Hamiltonian_MaxAngularMomentum_='',
                    Hamiltonian_MaxAngularMomentum_Si='"p"',
                    Hamiltonian_MaxAngularMomentum_H='"s"',
                    kpts=(1,1,1)
                    )

    system.set_calculator(calc)
    calc.calculate(system)

    slab = read('geo_end.gen')
    write('slab.final.xyz', slab)

    return slab


def getSlab():
    try:
        slab = read('slab.final.xyz')
        return slab
    except Exception as e:
        return None

if __name__ == '__main__':


    slab = getSlab()

    if not slab:
        # Build slab
        system = buildSystem()
        # Geometry Optimization of Hydrogens in Slab
        system.set_pbc([True, True, False])
        os.system('wsl rm dftb_* geo_end* geo_end*xyz results* *.out *final.*')
        slab = geOpt_H(system)


    write('slab.final1.xyz', slab)
    slab2= read('slab.final2.xyz')
    view(slab2)
    view(system)
    view(system,viewer='vmd') # change to CPK representation
