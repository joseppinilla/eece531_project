import os
import numpy as np
from math import sqrt
from copy import copy, deepcopy
# Atomic Simulation Environment (ASE)
from ase import Atoms
from ase.io import write, read
from ase.build import fcc100, diamond100, add_adsorbate, stack, root_surface, add_vacuum
from ase.geometry import get_layers
from ase.optimize import QuasiNewton
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.calculators.dftb import Dftb


def geOpt(system, MovedAtoms,i=0):

    calc = Dftb(    label='system',
                    atoms=system,
                    run_manyDftb_steps=True,
                    Driver_='ConjugateGradient',
                    Driver_MaxForceComponent='0.02',
                    Driver_MaxSteps=100,
                    Driver_MovedAtoms= MovedAtoms,
                    #Driver_KeepStationary='Yes',
                    #Driver_LatticeOpt='Yes',
                    #Driver_Isotropic='Yes',
                    #Hamiltonian_SCC='Yes',
                    Hamiltonian_MaxAngularMomentum_='',
                    Hamiltonian_MaxAngularMomentum_Si='"p"',
                    Hamiltonian_MaxAngularMomentum_H='"s"',
                    kpts=(1,1,1)
                    )

    system.set_calculator(calc)
    calc.calculate(system)

    opt = read('geo_end.gen')
    write('./xyz/system.final%s.xyz' % i, opt)

    return opt


tip = read('../structures/H_SiTip.xyz')
#view(tip)

slab = read('../workspace/slab.final1.xyz')
#view(slab)
slab.set_pbc([False, False, False])

slab_top = slab[360].position
x_init,y_init,z = slab_top
#z_layers = get_layers(slab, (0,0,1))
#slab_top = z_layers[1][-1]
#tip_low = slab_top + 0.5
tip.translate([x_init, y_init, z+1.5])

system = slab + tip
write('system.xyz',system)
#view(system)

#MovedAtoms
size = (6,6,10)
fixed_bot_layers = 2
# Move Si Atoms starting from the third layer
moved_first = fixed_bot_layers*size[0]*size[1]
# Move H atoms on top
moved_last =  (size[0]*size[1]*size[2]) + (size[0]*size[1]) - 1
move = str(moved_first) + ':' + str(moved_last)
# Atoms from tip
move += ' 468 470:473 475:478 480:489 492 493 495 501:503 508:510 513:531'

# Fix
# Fixed atoms in slab
fix = list(range(fixed_bot_layers*size[0]*size[1]))
fix.extend(list(range(396,467+1)))
fix.extend([469, 474, 479, 490, 491, 494, 496, 497, 498, 499, 500, 501, 504, 505, 506, 507, 512, 511])

c = FixAtoms(indices=fix)
system.set_constraint(c)
view(system)



tipH = 529
x_lim = slab[395].position[0]
y_lim = slab[395].position[1]

step = 0.1
i=0; dir=1; x=0; y=0;
while y < y_lim:
    on_x = True
    while on_x:
        os.system('wsl rm dftb_* geo_end* geo_end*xyz results* *.out *final*')
        system = geOpt(system, move, i)
        write('./images/slab%s.png' % i, system, rotation='4z,-85x')
        os.system('wsl mv results.tag ./results/results%s.tag' % i)
        os.system('wsl mv system.out ./out/system%s.out' % i)
        os.system('wsl mv band.out ./band/band%s.out' % i)
        # Atoms in Tip
        for atom in range(468,531):
            #Translate xxx
            system.positions[atom][0] = system.positions[atom][0] + (dir*step)
        i+=1
        x = system.positions[tipH][0]
        if dir<0:
            on_x = x > x_init
        else:
            on_x = x < x_lim
    os.system('wsl rm dftb_* geo_end* geo_end*xyz results* *.out *final*')
    system = geOpt(system, move, i)
    write('./images/slab%s.png' % i, system, rotation='4z,-85x')
    os.system('wsl mv results.tag ./results/results%s.tag' % i)
    os.system('wsl mv system.out ./out/system%s.out' % i)
    os.system('wsl mv band.out ./band/band%s.out' % i)
    # Atoms in Tip
    for atom in range(468,531):
        #Translate Y
        system.positions[atom][1] = system.positions[atom][1] + step
    y = system.positions[tipH][1]
    # Switch direction to scan X
    dir = -dir
    i+=1
