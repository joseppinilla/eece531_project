# FeO film on Pt(lll)
from ase.build import root_surface, fcc111, stack
from ase.visualize import view

primitive_substrate = fcc111('Pt', size = (1, 1, 4), vacuum =2)
view(primitive_substrate)
substrate = root_surface(primitive_substrate, 84)
view(substrate)
primitive_film = fcc111('Fe', size = (1, 1, 2), a=3.9, vacuum=1)
view(primitive_film)
primitive_film[1].symbol = 'O'
film = root_surface (primitive_film, 67)
view(film)
interface = stack (substrate, film, fix=0, maxstrain=None)
view(interface)
