import ase
from ase.build import cut, stack
from ase.visualize import view
from ase.spacegroup import crystal

# Create an Ag(110)-Si(110) interface with three atomic layers
# on each side.

a_ag = 4.09

ag = crystal(['Ag'], basis=[(0,0,0)], spacegroup=225,
          cellpar=[a_ag, a_ag, a_ag, 90., 90., 90.])

ag110 = cut(ag, (0, 0, 3), (-1.5, 1.5, 0), nlayers=3)

a_si = 5.43

si = crystal(['Si'], basis=[(0,0,0)], spacegroup=227,
          cellpar=[a_si, a_si, a_si, 90., 90., 90.])

si110 = cut(si, (0, 0, 2), (-1, 1, 0), nlayers=3)

interface = stack(ag110, si110, maxstrain=1)

view(interface)
# Once more, this time adjusted such that the distance between
# the closest Ag and Si atoms will be 2.3 Angstrom (requires scipy).

interface2 = stack(ag110, si110,
                maxstrain=1, distance=2.3)


view(interface2)
