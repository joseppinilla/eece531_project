


import numpy as np
from math import sqrt
from copy import copy, deepcopy
# Atomic Simulation Environment (ASE)
from ase import Atoms
from ase.io import write, read
from ase.build import fcc100, diamond100, add_adsorbate, stack, root_surface, add_vacuum
from ase.geometry import get_layers
from ase.optimize import QuasiNewton
from ase.visualize import view
from ase.constraints import FixAtoms
from ase.calculators.dftb import Dftb




def geOpt(system):

    calc = Dftb(    label='Tip',
                    atoms=system,
                    run_manyDftb_steps=True,
                    Driver_='ConjugateGradient',
                    Driver_MaxForceComponent='1E-4',
                    Driver_MaxSteps=200,
                    #Driver_MovedAtoms='H',
                    Hamiltonian_MaxAngularMomentum_='',
                    Hamiltonian_MaxAngularMomentum_Si='"p"',
                    Hamiltonian_MaxAngularMomentum_H='"s"',
                    kpts=(1,1,1)
                    )

    system.set_calculator(calc)
    calc.calculate(system)

    slab = read('geo_end.gen')
    write('tip.final.xyz', slab)

    return slab


tip = read('../structures/H_SiTip.xyz')
view(tip)

opt=geOpt(tip)
